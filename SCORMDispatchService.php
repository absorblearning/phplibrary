<?php

require_once ('Configuration.php');
require_once ('ServiceRequest.php');
require_once ('CourseService.php');
require_once ('DispatchService.php');
require_once ('InvitationService.php');

class SCORMDispatchService{

	private $_configuration = null;
    private $_courseService = null;
	private $_serviceRequest = null;
    private $_dispatchService = null;
    private $_invitationService = null;

	public function __construct($scormServiceUrl, $appId, $securityKey) {
		$this->_configuration = new Configuration($scormServiceUrl, $appId, $securityKey);
		$this->_serviceRequest = new ServiceRequest($this->_configuration);
        $this->_courseService = new CourseService($this->_configuration);
        $this->_dispatchService = new DispatchService($this->_configuration);
        $this->_invitationService = new InvitationService($this->_configuration);
	}
	
    public function isValidAccount() {
        $appId = $this->getAppId();
        $key = $this->getSecurityKey();
        $url = $this->getSCORMDispatchServiceUrl();
        
        if (empty($appId) || empty($key) || empty($url)) {
            return false;
        }
        
        return true;
    }   
    

    public function getCourseService()
    {
        return $this->_courseService;
    }

    public function getAppId()
    {
            return $this->_configuration->getAppId();
    }


    public function getSecurityKey()
    {
            return $this->_configuration->getSecurityKey();
    }

	
    public function getSCORMDispatchServiceUrl()
    {
            return $this->_configuration->getSCORMDispatchServiceUrl();
    }    

   
    public function getDispatchService()
    {
        return $this->_dispatchService;
    }

  
    public function getInvitationService()
    {
        return $this->_invitationService;
    }
    

    public function CreateNewRequest()
    {
        return new ServiceRequest($this->_configuration);
    }
}
?>
