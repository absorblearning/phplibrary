<?php

class CourseData{
	private $_courseId;
    private $_scormId;
    private $_version;
    private $_title;
    private $_addedDate;

    private $_numberOfRegistrations;
    private $_numberOfVersions;
    


    public function __construct($courseDataElement)
    {
		if(isset($courseDataElement))
		{
            $this->_courseId = (string) $courseDataElement['id'];
            $this->_scormId = (string) $courseDataElement['scorm_id'];
            $this->_version = (string) $courseDataElement['version'];
            $this->_addedDate = (string) $courseDataElement['created_at'];
            $this->_title = (string) $courseDataElement['title'];
		}
    }


    public static function ConvertToCourseDataList($data)
    {   
        $allResults = array();
		if (false == $data['data']['status']) {
            return $allResults;
        }		
        
        foreach ($data['data']['courses'] as $course)
        {   
            $allResults[] = new CourseData($course);
        }
        
        return $allResults;
    }

    public function getCourseId()
    {
        return $this->_courseId;
    }


    public function getNumberOfVersions()
    {
        return $this->_numberOfVersions;
    }


    public function getNumberOfRegistrations()
    {
        return $this->_numberOfRegistrations;
    }


    public function getTitle()
    {
        return $this->_title;
    }


    public function getAddedDate()
    {
        return $this->_addedDate;
    }


    public function getSCORMId()
    {
        return $this->_scormId;
    }


    public function getVersion()
    {
        return $this->_version;
    }       
}

?>
