<?php

require_once 'Configuration.php';

class ServiceRequest{
	

    const TIMEOUT_CONNECTION = 30;
  
    const TIMEOUT_TOTAL = 500;
	
	private $_configuration = null;
	private $_methodParams = array();
	private $_fileToPost = null;
    private $_dataToPost = null;

	public function __construct($configuration) {
		$this->_configuration = $configuration;
	}
	
	public function setMethodParams($paramsArray)
	{
		$this->_methodParams = array_merge($this->_methodParams, $paramsArray);
	}
	
	public function setFileToPost($fileName)
	{
		$this->_fileToPost = $fileName;
	}
	
    public function setDataToPost($dataName)
    {
        $this->_dataToPost = $dataName;
    }

	public function CallService($methodName)
	{
		$postParams = null;
		if(isset($this->_fileToPost))
		{
			//TODO - check to see if this file exists
			if (!function_exists('curl_file_create')) {
				// PHP version older than 5.5
				$postParams = array('filedata' => "@$this->_fileToPost");
			} else {
				// PHP 5.5 and higher uses curl_file_create.
				$postParams = array('filedata' => curl_file_create($this->_fileToPost));
			}
		}

        if(isset($this->_dataToPost))
        {        
            $postParams = $this->_dataToPost;
        }

		$url = $this->ConstructUrl($methodName);
        
		$responseText = $this->submitHttpPost($url,$postParams, self::TIMEOUT_TOTAL);
        $response = json_decode($responseText, true);
        return $response;
	}

    public function ConstructUrl($methodName)
    {
        return $this->ConstructAppAgnosticUrl($this->_configuration->getAppId(), 
                                        $this->_configuration->getSecurityKey(), 
                                        $methodName);
    }
	
	public function ConstructAppAgnosticUrl($appId, $secretKey, $methodName)
	{
        $sig = md5($secretKey . $appId);
        $parameterMap = array(  $methodName,
                                $appId,
                                $sig
                            );

        $params = array();
		array_merge($params, $this->_methodParams);
		foreach($this->_methodParams as $key => $value)
		{
			$params[$key] = $value;            
		}       
        
        $url = $this->_configuration->getSCORMDispatchServiceUrl();
        $p = $this->signMainParams($secretKey,$parameterMap);
        $params = $this->signParams($secretKey,$params);     

        $url .= '/' . $p;
        if ($params != '')
            $url .= '?' . $params;

		return $url;
	}
	
    static function submitHttpPost($url, $postParams = null,  $timeout = self::TIMEOUT_TOTAL)
    {
        $ch = curl_init();
        
        
        curl_setopt($ch, CURLOPT_URL, $url);
       
        curl_setopt($ch, CURLOPT_POST, 1);
        if (isset($postParams)) {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
        } else{
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_POSTFIELDS, "");        	
        }
        // make sure problems are caught
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        // return the output
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // set the timeouts
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::TIMEOUT_CONNECTION);
        curl_setopt($ch, CURLOPT_TIMEOUT,$timeout);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));

       
        set_time_limit(self::TIMEOUT_CONNECTION + $timeout + 5);

        $result = curl_exec($ch);
       
        if (0 == curl_errno($ch)) {
            curl_close($ch);
            return $result;
        } else {
            echo ('Request to '.$url.' failed. ERROR: '.curl_error($ch));
            curl_close($ch);
        }
        
    }    

   
    static function signParams($secret, $params)
    {
        $signing = '';
        $values = array();
        if (defined('SORT_FLAG_CASE')) { //PHP 5.4 and higher only.
            ksort($params, SORT_STRING | SORT_FLAG_CASE);
        } else {
            ksort($params, SORT_STRING);
        }

        foreach($params as $key => $value) {
            $values[] = $key . '=' . urlencode($value);
            //$values[] = $key . '=' . $value;
        }       
        return implode('&', $values);
    }

    static function signMainParams($secret, $params)
    {
        $signing = '';
        $values = array();
        if (defined('SORT_FLAG_CASE')) { //PHP 5.4 and higher only.
            ksort($params, SORT_STRING | SORT_FLAG_CASE);
        } else {
            ksort($params, SORT_STRING);
        }

        foreach($params as $key => $value) {            
            $values[] = $value;
        }       
        return implode('/', $values);
    }

}

?>
