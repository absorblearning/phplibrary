<?php

class DispatchDestination {
    private $_id;
    private $_name;
    private $_createdBy;
    private $_createDate;
    private $_updateDate;

    public function __construct($data)
    {
		if(isset($data))
		{

	        $this->_id = (string) $data['id'];
	        $this->_name = (string) $data['name'];
	        $this->_createdBy = (string) $data['creator_id'];
	        $this->_createDate = (string) $data['created_at'];
	        $this->_updateDate = (string) $data['updated_at'];
		}
    }
    
    public static function parseDestinationList($data)
    {
        $allResults = array();

        if (false == $data['data']['status']) {
            return $allResults;    
        }
        
        foreach ($data['data']['destinations'] as $destination)
        {   
            $allResults[] = new DispatchDestination($destination);
        }
        
        return $allResults;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getCreatedBy()
    {
        return $this->_createdBy;
    }

    public function getCreateDate()
    {
        return $this->_createDate;
    }

    public function getUpdateDate()
    {
        return $this->_updateDate;
    }
}
?>
