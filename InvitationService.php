<?php

require_once 'ServiceRequest.php';
require_once 'InvitationData.php';


class InvitationService{
	
	private $_configuration = null;
	
	public function __construct($configuration) {
		$this->_configuration = $configuration;
		//echo $this->_configuration->getAppId();
	}

    public function CreateInvitation($courseId, $subject, $emails)
    {
		$request = new ServiceRequest($this->_configuration);
        $params = array('cid' => $courseId);

		if (isset($emails))
		{
            $params['emails'] = $emails;
		}
		if (isset($subject))
		{
            $params['subject'] = $subject;
		}
		
		//$request->setMethodParams($params);
        $request->setDataToPost($params);
        
        //return $request->CallService("course/importCourse");
        
		$response = $request->CallService("invitation/createInvitation");
		
        return $response;
    }

	
    public function UpdateInvitation($invitationId, $status, $registration)
    {
		$request = new ServiceRequest($this->_configuration);
        $params = array('inviteId' => $invitationId);

		if (isset($registration))
		{
            $params['maxreg'] = $registration;
		}
		if (isset($status))
		{
            $params['status'] = $status;
		}
		
		$request->setMethodParams($params);

		$response = $request->CallService("invitation/updateInvitation");
		
        return $response;
    }

	public function DeleteInvitation($invitationId)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('inviteId' => $invitationId);
		$request->setMethodParams($params);
        return $request->CallService("invitation/deleteInvitation");
    }

	public function GetInvitationList()
    {
		$request = new ServiceRequest($this->_configuration);
        $response = $request->CallService("invitation/getInvitations");

        $invitationData = new InvitationData(null);
        $invitationArray = $invitationData->ConvertToInvitationList($response);

        return $invitationArray;
    }

	public function GetInvitationStatus($invitationId)
    {
		$request = new ServiceRequest($this->_configuration);
		$params = array();
		
		$params['inviteId'] = $invitationId;		
        
		$request->setMethodParams($params);
        $response = $request->CallService("invitation/getStatus");
        return $response;
    }
	

	public function GetInvitationInfo($invitationId)
    {
		$request = new ServiceRequest($this->_configuration);
		$params = array();
		
		$params['inviteId'] = $invitationId;
        
		$request->setMethodParams($params);
        $response = $request->CallService("invitation/getInvitation");
        $invitationData = new InvitationData($response['data']['invitation']);
        
        return $invitationData;
    }

	public function ChangeStatus($invitationId, $enable)
    {
		$request = new ServiceRequest($this->_configuration);
		$params = array();
		
		$params['inviteId'] = $invitationId;
		$params['status'] = $enable;		
        
		$request->setMethodParams($params);
        $response = $request->CallService("invitation/changeStatus");
        return $response;
    }
    
    
 }

?>
