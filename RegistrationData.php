<?php

class RegistrationData
{
    private $_registrationId;
    private $_courseId;
    private $_courseTitle;
	private $_data;



    public function __construct($registrationDataElement)
    {
        $this->_registrationId = (string) $registrationDataElement['regid'];
        $this->_courseId = (string) $registrationDataElement['course'];
        $this->_courseTitle = (string) $registrationDataElement['courseTitle'];
    }

  
    public static function ConvertToRegistrationDataList($data)
    {		
        $allResults = array();

        if (false == $data['data']['status']) {
            return $allResults;
        }
        
        foreach ($data['data']['registrations'] as $registration)
        {
            
            $allResults[] = new RegistrationData($registration);
        }
        
        return $allResults;
    }

   
    public function getRegistrationId()
    {
        return $this->_registrationId;
    }

   
    public function getCourseId()
    {
        return $this->_courseId;
    }

    public function getCourseTitle()
    {
        return $this->_courseTitle;
    }

	public function getData()
    {
        return $this->_data;
    } 
}

?>
