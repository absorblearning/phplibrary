<?php

class LaunchHistory
{
    private $_score;
    private $_completion;
    private $_satisfaction;
    private $_totaltime;
    private $_attempt;
    private $_coursetitle;


    public function __construct($report)
    {
		$this->_completion = (string) $report['complete_status'];
        $this->_satisfaction = (string) $report['satisfied_status'];
        $this->_score = (string) $report['score'];
        $this->_totaltime = (string) $report['total_time'];
        $this->_attempt =  (string) $report['attempt'];
        $this->_coursetitle =  (string) $report['courseTitle'];
    }



    public function getScore()
    {
        return $this->_score;
    }


    public function getCompletion()
    {
		return $this->_completion;
    }

 
    public function getSatisfaction()
    {
        return $this->_satisfaction;
    }


    public function getTotalTime()
    {
        return $this->_totaltime; 
    }


    public function getAttempt()
    {
        return $this->_attempt; 
    }


    public function getCourseTitle()
    {
        return $this->_coursetitle; 
    }


	public static function ConvertToLaunchInfoList($data)
    {
        $allResults = array();

        if (false == $data['data']['status']) {
            return $allResults;
        }

        foreach ($data['data']['reports'] as $report)
        {
            $allResults[] = new LaunchHistory($report);
        }

        return $allResults;
    }
}
?>