<?php

class Token {

    private $_tokenId;


    public function __construct($tokenData)
    {
        if (false == $tokenData['data']['status']) {
            return false;
        }  
		if(isset($tokenData))
		{
	        $this->_tokenId = $tokenData['data']['token'];
		}
    }

  
    public function getTokenId()
    {
        return $this->_tokenId;
    }

}

?>