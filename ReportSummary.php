<?php

class ReportSummary
    {
		private $_complete = 'unknown';
        private $_success = 'unknown';
        private $_totalTime = '0s';
        private $_score = 'unknown';

	
        public function __construct($data)
        {

            if (false == $data['data']['status']) {
                return false;
            }         
            
            foreach ($data['data']['reports'] as $report)
            {
                $this->_complete = $report['complete_status'];
                $this->_success = $report['satisfied_status'];
                $this->_totalTime = $report['total_time'];
                $this->_score = $report['score'];            
            }
        }

	
        public function getComplete()
        {
            return $this->_complete;
        }


        public function getSuccess()
        {
			return $this->_success;
        }


        public function getTotalTime()
        {
            return $this->_totalTime;
        }

   
        public function getScore()
        {
           return $this->_score;
        }

}

?>
