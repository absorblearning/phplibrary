<?php

class DispatchReports {
    private $_id;
    private $_name;
    private $_learner;
    private $_complete_status;
    private $_satisfied_status;
    private $_score;
    private $_total_time;
    private $_attempt;
    private $_createDate;
    private $_updateDate;

    public function __construct($data)
    {
		if(isset($data))
		{
	        $this->_id = (string) $data['id'];
            //$this->_name = (string) $data['name'];
            $this->_learner = (string) $data['learner'];
	        $this->_complete_status = (string) $data['complete_status'];
	        $this->_satisfied_status = (string) $data['satisfied_status'];
	        $this->_score = (string) $data['score'];
	        $this->_total_time = (string) $data['total_time'];
	        $this->_attempt = (string)$data['attempt'];
	        $this->_createDate = (string) $data['created_at'];
	        $this->_updateDate = (string) $data['updated_at'];
		}
    }
    
    public static function parseDispatchReports($data)
    {
        $allResults = array();

		if (false == $data['data']['status']) {
            return $allResults;
        }
        
        foreach ($data['data']['histories'] as $dispatchElem)
        {
            $allResults[] = new DispatchReports($dispatchElem);
        }

        return $allResults;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getLearner()
    {
        return $this->_learner;
    }

    public function getCompletedStatus()
    {
        return $this->_complete_status;
    }

    public function getSatisfiedStatus()
    {
        return $this->_satisfied_status;
    }

    public function getScore()
    {
        return $this->_score;
    }

    public function getAttempt()
    {
        return $this->_attempt;
    }

    public function getTotalTime()
    {
        return $this->_total_time;
    }
    
    public function getCreateDate()
    {
        return $this->_createDate;
    }

    public function getUpdateDate()
    {
        return $this->_updateDate;
    }
}
?>
