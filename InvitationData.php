<?php

class InvitationData
    {
        private $_id;
        private $_subject;
	    private $_courseId;
        private $_courseTitle;
        private $_email;
        private $_status;
        private $_max_registration;
        private $_created_at;
        private $_updated_at;        

		
        public function __construct($data)
        {
            $this->_id = (string) $data['id'];
            $this->_subject = (string) $data['subject'];
            $this->_courseId = (string) $data['course_id'];
            $this->_courseTitle = (string) $data['courseTitle'];
            $this->_email = (string) $data['email'];
            $this->_status =  (string) $data['status'];
            $this->_max_registration =  (string) $data['max_registration'];
            $this->_created_at =  (string) $data['created_at'];
            $this->_updated_at =  (string) $data['updated_at'];
        }


		
        public function getId()
        {
            return $this->_id;
        }

       
        public function getSubject()
        {
            return $this->_subject;
        }

     
        public function getCourseId()
        {
			return $this->_courseId;
        }

    
        public function getCourseTitle()
        {
            return $this->_courseTitle;
        }

   
        public function getEmail()
        {
            return $this->_email; 
        }

 
        public function getStatus()
        {
            return $this->_status; 
        }
 
    
        public function getMaxRegistration()
        {
            return $this->_max_registration; 
        }

    
        public function getCreatedDate()
        {
            return $this->_created_at; 
        }

   
        public function getUpdatedDate()
        {
            return $this->_updated_at; 
        }        

		public static function ConvertToInvitationList($data)
        {
            $allResults = array();

            if (false == $data['data']['status']) {
                return $allResults;
            }

            foreach ($data['data']['invitations'] as $invitation)
            {
                $allResults[] = new InvitationData($invitation);
            }

            return $allResults;
        }
}

?>
