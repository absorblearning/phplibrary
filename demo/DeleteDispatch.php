<?php

	require_once('config.php');
	require_once('../SCORMDispatchService.php');

	global $CFG;

	$ServiceUrl = $CFG->serviceurl;
	$AppId = $CFG->appid;
	$SecretKey = $CFG->secretkey;

	$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
	$dispService = $ScormService->getDispatchService();
	if (isset($_GET['id'])) {
		$dispService->DeleteDispatches($_GET['id']);
	}

	header('Location: ManagementDispatch.php') ;
?>