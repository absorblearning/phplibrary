<?php
require_once('config.php');
require_once('../SCORMDispatchService.php');

global $CFG;

$ServiceUrl = $CFG->serviceurl;
$AppId = $CFG->appid;
$SecretKey = $CFG->secretkey;

$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
$courseService = $ScormService->getCourseService();
$regid = $_GET['regid'];
$allResults = $courseService->GetLaunchHistory($regid);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>History</title>
	
</head>

<body>
<?php
	echo '<table border="1" cellpadding="5">';
	echo '<tr><td>Course</td><td>Completion</td><td>Satisfaction</td><td>Total Time</td><td>Launch Time</td><td>Attempt</td></tr>';

	foreach($allResults as $result)
	{
		echo '<tr><td>';
		echo $result->getCourseTitle();
		echo '</td><td>';
		echo $result->getCompletion();
		echo '</td><td>';
		echo $result->getSatisfaction();
		echo '</td><td>';
		echo $result->getTotalTime();
		echo '</td><td>';
		echo $result->getScore();
		echo '</td><td>';
		echo $result->getAttempt();
		echo '</td><tr>';

	}
	
	echo '</table>';

?>
<h3><a href="index.php">Go to Main Page.</a></h3>
</body>
</html>