<?php

	require_once('config.php');
	require_once('../SCORMDispatchService.php');

	global $CFG;

	$ServiceUrl = $CFG->serviceurl;
	$AppId = $CFG->appid;
	$SecretKey = $CFG->secretkey;


	$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);

	$dispService = $ScormService->getDispatchService();

	$allDispatchReports = $dispService->GetDispatchReports($_GET['id']);
	$total_count = count($allDispatchReports);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Management Dispatch</title>	
</head>

<?php

$launchUrl = '';
$i = 1;
echo '<h3>Report of Learners (' . $total_count . ')</h3>';
echo '<table border="1" cellpadding="0" cellspacing="0">';
echo '<tr><th>No.</th><th>Learner</th><th>Completion</th><th>Success</th><th>Score</th><th>Total Time</th><th>Attempt</th><th>Created Date</th><th>Updated Date</th></tr>';

foreach($allDispatchReports as $result)
{	
	echo '<tr>';
	echo '<td>' . $i . '</td>';
	echo '<td>' . $result->getLearner() . '</td>';
	echo '<td>' . $result->getCompletedStatus() . '</td>';
	echo '<td>' . $result->getSatisfiedStatus() . '</td>';
	echo '<td>' . $result->getScore() . '</td>';
	echo '<td>' . $result->getTotalTime() . '</td>';
	echo '<td>' . $result->getAttempt() . '</td>';
	echo '<td>' . $result->getCreateDate() . '</td>';
	echo '<td>' . $result->getUpdateDate() . '</td>';
	echo '</tr>';

	$i ++;
}
echo '</table><br/><br/>';

$reportUrl = "";
echo '<h3><a href="ManagementDispatch.php">Go to Dispatch Page.</a></h3>';
?>
</body>
</html>