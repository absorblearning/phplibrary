<?php
require_once('config.php');
require_once('../SCORMDispatchService.php');

global $CFG;

$ServiceUrl = $CFG->serviceurl;
$AppId = $CFG->appid;
$SecretKey = $CFG->secretkey;

$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
$courseService = $ScormService->getCourseService();

$allResults = $courseService->GetCourseList();
$redirectUrl = $CFG->wwwroot . 'CourseList.php';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>Course List</title>
	
</head>

<body>

	<table width="50%" border="1" cellpadding="=" cellspacing="0">
		<tr>
			<th>No.</th>
			<th>Title</th>
			<th>Version</th>
			<th>Created Date</th>
			<th>Course Status</th>
			<th>Action</th>
		</tr>
		<?php
		$i = 1;
foreach($allResults as $course) {	
		?>		
		<tr>
			<td><?=$i++?></td>
			<td><?=$course->getTitle()?></td>
			<td><?=$course->getVersion()?></td>
			<td><?=$course->getAddedDate()?></td>
			<td>
				<?php
					$summary = $courseService->GetCourseSummary($course->getCourseId(), $redirectUrl);
				?>
				<table>
					<tr>
						<th>Completion</th>
						<th>Success</th>
						<th>Score</th>
						<th>Total Time</th>
					</tr>
					<tr>
						<td><?=$summary->getComplete()?></td>
						<td><?=$summary->getSuccess()?></td>
						<td><?=$summary->getScore()?></td>
						<td><?=$summary->getTotalTime()?></td>
					</tr>
				</table>
			</td>
			<td>
				<?php
					$launch_url = $courseService->GetPreviewUrl($course->getCourseId(), $redirectUrl);					
				?>
				
				<div><a href="<?=$launch_url?>">Launch</a> || <a href="DeleteCourse.php?id=<?=$course->getCourseId()?>">Delete</a></div>
			</td>
		</tr>
		<?php
}
		?>
	</table>
<h3><a href="index.php">Go to Main Page.</a></h3>
</body>
</html>