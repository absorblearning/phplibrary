<?php

	require_once('config.php');
	require_once('../SCORMDispatchService.php');

	global $CFG;

	$ServiceUrl = $CFG->serviceurl;
	$AppId = $CFG->appid;
	$SecretKey = $CFG->secretkey;

	$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
	$courseService = $ScormService->getCourseService();
	if (isset($_GET['id'])) {
		//$delete_url = $courseService->DeleteCourse($course->getCourseId(), $redirectUrl);
		$result = $courseService->DeleteCourse($_GET['id']);
		
		//header('Location: CourseList.php') ;
		if ($result['data']['status'] == true)
			header('Location: CourseList.php') ;
		else
			var_dump($result['data']['status']);
	}

	
?>