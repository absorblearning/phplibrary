<?php

require_once('config.php');
require_once('../SCORMDispatchService.php');

global $CFG;

$ServiceUrl = $CFG->serviceurl;
$AppId = $CFG->appid;
$SecretKey = $CFG->secretkey;


$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
$dispService = $ScormService->getDispatchService();

$allResults = array();
if(isset($_GET['name'])){
	$destname = $_GET['name'];
	$result = $dispService->CreateDestination($destname);
	if ($result['data']['status'] == true) {
		$id = $result['data']['id'];
		header('Location: ManagementDestination.php?id='.$id) ;
	} else {
		echo "It's failed while create new destination.";
	}

} else {
	
}

?>