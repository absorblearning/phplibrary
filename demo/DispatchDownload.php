<?php

	require_once('config.php');
	require_once('../SCORMDispatchService.php');

	global $CFG;

	$ServiceUrl = $CFG->serviceurl;
	$AppId = $CFG->appid;
	$SecretKey = $CFG->secretkey;

	$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
	$dispService = $ScormService->getDispatchService();

	if (isset($_GET['download']) && $_GET['download'] == 'Download') {
		if (isset($_GET['dispatch']) && isset($_GET['learning_standards'])) {

			$url = $dispService->DownloadDispatchPackage($_GET['dispatch'], $_GET['learning_standards']);
			header('Location: ' . $url);
		}
	}
?>