<?php

	require_once('config.php');
	require_once('../SCORMDispatchService.php');

	global $CFG;

	$ServiceUrl = $CFG->serviceurl;
	$AppId = $CFG->appid;
	$SecretKey = $CFG->secretkey;

	$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
	$invService = $ScormService->getInvitationService();
	if (isset($_GET['id']) && isset($_GET['status'])) {
		$invService->ChangeStatus($_GET['id'], $_GET['status']);
	}
	
	header('Location: ManagementInvitation.php') ;
?>