<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>Courseware System</title>
	
</head>

<body>

<div> 
<ul style="list-style-type:circle">
  <li><a href="CourseList.php">Course List, Launch and Summary(general)</a></li>
  <li><a href="RegistrationList.php">Management course by registration</a></li>
  <li><a href="CourseUploadAsync.php">Course upload (Async)</a></li>
  <li><a href="CourseUploader.php">Course uploader (form)</a></li>  
  <li><a href="ManagementDestination.php">Management destination</a></li>
  <li><a href="ManagementDispatch.php">Management dispatch for course</a></li>
  <li><a href="ManagementInvitation.php">Management invitation</a></li>  
</ul>
</div>	

</body>
</html>