<?php

	require_once('config.php');
	require_once('../SCORMDispatchService.php');

	global $CFG;

	$ServiceUrl = $CFG->serviceurl;
	$AppId = $CFG->appid;
	$SecretKey = $CFG->secretkey;


	$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);

	$courseService = $ScormService->getCourseService();
	$allCourseResults = $courseService->GetCourseList();

	$dispService = $ScormService->getDispatchService();
	$allResults = $dispService->GetDestinationList();

	$allDispatchResults = $dispService->GetDispatchList();

	if (isset($_GET['id'])) {
		
		$id = $_GET['id'];
		$result = $dispService->GetDispatchInfo($id);
		
		echo '<h3>Details of Dispatch</h3>';
		echo '<table border="1" cellpadding="1" cellspacing="1">';
		echo '<tr><td>Name</td><td>Destination</td><td>Course</td><td>Created Date</td><td>Updated Date</td></tr>';
		echo '<tr><td>' . $result->getName() . '</td>';
		echo '<td>' . $result->getDestinationName() . '</td>';
		echo '<td>' . $result->getCourseTitle() . '</td>';
		echo '<td>' . $result->getCreateDate() . '</td>';
		echo '<td>' . $result->getUpdateDate() . '</td></tr>';
		echo '</table><br/><br/>';
		
	} 

	echo '<form action="CreateDispatch.php" method="GET">';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>Management Dispatch</title>
	
</head>

<body>

<h3>Create New Dispatch</h3>
<div>Course Name: 
	<select name='cid'>
		<?php		
			foreach($allCourseResults as $course) {
				echo '<option value=' . $course->getCourseId() . '>' . $course->getTitle() . '</option>';
			}
		?>
	</select>
</div>
<div>Destination Name: 
	<select name='did'>
		<?php		
			foreach($allResults as $destination) {
				echo '<option value=' . $destination->getId() . '>' . $destination->getName() . '</option>';
			}
		?>		
	</select>
</div>
<div>Dispatch Name:<input type="text" name="name" /></div>
<div>Access Limit:<input type="text" name="limit" /></div>
<div><input type="submit" name="submit" value="Submit" /></div>
</form>
</body>
<br/><br/>
<br/><br/>
<?php

$launchUrl = '';
$i = 1;
echo '<h3>List of Dispatch</h3>';
echo '<table border="1" cellpadding="1" cellspacing="1">';
echo '<tr><td>No.</td><td>Name</td><td>Destination</td><td>Course</td><td>Access Limit</td><td>Created Date</td><td>Updated Date</td><td>Download</td><td>Report</td><td>Action</td></tr>';

foreach($allDispatchResults as $result)
{	
	echo '<tr>';
	echo '<td>' . $i . '</td>';
	echo '<td>' . $result->getName() . '</td>';
	echo '<td>' . $result->getDestinationName() . '</td>';
	echo '<td>' . $result->getCourseTitle() . '</td>';
	echo '<td>' . $result->getLimit() . '</td>';
	echo '<td>' . $result->getCreateDate() . '</td>';
	echo '<td>' . $result->getUpdateDate() . '</td>';

	echo '<td>';
	
	echo '<form action="DispatchDownload.php" method="GET">';	
	echo '<input type="hidden" name="dispatch" value="'. $result->getId() .'">';
	echo '<div><b>Learning type</b>';
	echo '	<select name="learning_standards">';
	echo '			<option value="scorm_12">SCORM 1.2</option>';
	echo '			<option value="scorm_13">SCORM 2004</option>';
	echo '		</select>';
	echo '	</div>';
	echo '<div><input type="submit" name="download" value="Download"></div>';
	echo '</form>';

	echo '<a href='. $dispService->DownloadDispatchPackage($result->getId(), 'scorm_12') .'>Download</a></td>';
	echo '</td>';
	echo '<td><a href=ReportDispatch.php?id='. $result->getId() .'>Report History</a></td>';
	echo '<td><a href=DeleteDispatch.php?id='. $result->getId() .'>Delete</a></td>';
	echo '</tr>';

	$i ++;
}
echo '</table><br/><br/>';

$reportUrl = "";
echo '<h3><a href="index.php">Go to Main Page.</a></h3>';
?>
</body>
</html>