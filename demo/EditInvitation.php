<?php

	require_once('config.php');
	require_once('../SCORMDispatchService.php');

	global $CFG;

	$ServiceUrl = $CFG->serviceurl;
	$AppId = $CFG->appid;
	$SecretKey = $CFG->secretkey;

	$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
	$invService = $ScormService->getInvitationService();
	if (isset($_GET['id'])) {
		$invitation = $invService->GetInvitationInfo($_GET['id']);

		$invitation->getId();
		$invitation->getSubject();
		$invitation->getCourseTitle();
		$invitation->getEmail();
		$invitation->getStatus();
		$invitation->getMaxRegistration();

		$invitation->getCreatedDate();
		$invitation->getUpdatedDate();
	} else if ($_POST['action'] == 'Update'){
		$invitationId = $_POST['id'];
		if (isset($_POST['status']) && $_POST['status'] == 'on')
			$status = 1;
		else
			$status = 0;
		$max_reg = $_POST['max_reg'];

		$result = $invService->UpdateInvitation($invitationId, $status, $max_reg);
		if ($result['data']['status'])
			header('Location: ManagementInvitation.php?invid='.$invitationId) ;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>Management Invitation</title>
	
</head>

<body>
<form action="EditInvitation.php" method="POST">
	<input type="hidden" name="id" value="<?=$invitation->getId()?>">
<h3>Edit Invitation</h3>
<table>
	<tr><td>
		Subject : 
	</td><td>
	<?=$invitation->getSubject()?>	
	</td></tr>	
	<tr><td>
		Course Name : 
	</td><td>
	<?=$invitation->getCourseTitle()?>	
	</td></tr>	
	<tr><td>
		Emails :
	</td><td>
		<?=$invitation->getEmail()?>	
	</td></tr>
	<tr><td>
		Status :
	</td><td>
		<?php 
		if ($invitation->getStatus()) {
			echo "Enabled";
		} else {
			echo "Disabled";
		}
		?>

		<input type="checkbox" name="status" <?php echo ($invitation->getStatus()==1 ? 'checked' : '');?>>
	</td></tr>
	<tr><td>
		Max registrations :
	</td><td>
		<input type="text" name="max_reg" value="<?=$invitation->getMaxRegistration()?>">
	</td></tr>		
</table>
<div><input type="submit" name="action" value="Update" /></div>


</form>
</body>

</html>
