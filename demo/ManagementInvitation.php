<?php

	require_once('config.php');
	require_once('../SCORMDispatchService.php');

	global $CFG;

	$ServiceUrl = $CFG->serviceurl;
	$AppId = $CFG->appid;
	$SecretKey = $CFG->secretkey;


	$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);

	$invService = $ScormService->getInvitationService();
	$courseService = $ScormService->getCourseService();
	$allCourseResults = $courseService->GetCourseList();
	$invitations = $invService->GetInvitationList();

	if (isset($_GET['id'])) {
		
		$id = $_GET['id'];
		$result = $invService->GetInvitationInfo($id);
		
			echo '<h3>Details of Invitation</h3>';
			echo '<table border="1" cellpadding="1" cellspacing="1">';
			echo '<tr><td>Subject</td><td>Course</td><td>Email</td><td>Status</td><td>Max Registration</td><td>Created Date</td><td>Updated Date</td></tr>';
			echo '<tr><td>' . $result->getSubject() . '</td>';
			echo '<td>' . $result->getCourseTitle() . '</td>';
			echo '<td>' . $result->getEmail() . '</td>';
			echo '<td>';

				if ($result->getStatus() == 1)
					echo 'Enabled';
				else
					echo 'Disabled';
				
			echo '</td>';
			echo '<td>' . $result->getMaxRegistration() . '</td>';
			echo '<td>' . $result->getCreatedDate() . '</td>';
			echo '<td>' . $result->getUpdatedDate() . '</td></tr>';
			echo '</table><br/><br/>';	
		
	} 

	echo '<form action="CreateInvitation.php" method="POST">';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>Management Invitation</title>
	
</head>

<body>
<form action="CreateInvitation.php" method="POST">
<h3>Create New Invitation</h3>
<table>
	<tr><td>
		Course Name: 
	</td><td>
	<select name='cid'>
		<?php		
			foreach($allCourseResults as $course) {
				echo '<option value=' . $course->getCourseId() . '>' . $course->getTitle() . '</option>';
			}
		?>
	</select>		
	</td></tr>
	<tr><td>
		Subject
	</td><td>
		<input type="text" name="subject">
	</td></tr>
	<tr><td>
		Emails to invitation: 
	</td><td>
		<textarea class="form-control" rows="5" id="email" name="email"></textarea>
		* Enter User Emails:(comma separated)
	</td></tr>
</table>
<div><input type="submit" name="submit" value="Submit" /></div>


</form>
</body>
<br/><br/>
<br/><br/>
<?php

$launchUrl = '';
$i = 1;
echo '<h3>List of Dispatch</h3>';
echo '<table border="1" cellpadding="1" cellspacing="1">';
echo '<tr><td>No.</td><td>Subject</td><td>Course</td><td>Emails</td><td>Status</td><td>Max Registrations</td><td>Created Date</td><td>Updated Date</td><td>Action</td></tr>';
foreach($invitations as $invitation)
{	
	echo '<tr>';
	echo '<td>' . $i . '</td>';
	echo '<td>' . $invitation->getSubject() . '</td>';
	echo '<td>' . $invitation->getCourseTitle() . '</td>';
	echo '<td>' . $invitation->getEmail() . '</td>';
	echo '<td>'; 
	if ($invitation->getStatus() == 1) {
		echo '<a href=InvitationChangeStatus.php?id='. $invitation->getId() .'&status=0>Enabled</a>';
	} else {
		echo '<a href=InvitationChangeStatus.php?id='. $invitation->getId() .'&status=1>Disabled</a>';
	}
	echo '</td>';
	echo '<td>' . $invitation->getMaxRegistration() . '</td>';
	echo '<td>' . $invitation->getCreatedDate() . '</td>';
	echo '<td>' . $invitation->getUpdatedDate() . '</td>';


	echo '<td><a href=DeleteInvitation.php?id='. $invitation->getId() .'>Delete</a> || <a href=EditInvitation.php?id='. $invitation->getId() .'>Edit</a></td>';
	echo '</tr>';

	$i ++;
}
echo '</table><br/><br/>';

$reportUrl = "";
echo '<h3><a href="index.php">Go to Main Page.</a></h3>';
?>
</body>
</html>