<?php

require_once('config.php');
require_once('../SCORMDispatchService.php');

global $CFG;

$ServiceUrl = $CFG->serviceurl;
$AppId = $CFG->appid;
$SecretKey = $CFG->secretkey;


$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
$invService = $ScormService->getInvitationService();

$allResults = array();
if(isset($_POST['cid']) && isset($_POST['subject']) && isset($_POST['email'])){
	$courseid = $_POST['cid'];
	$subject = $_POST['subject'];
	$email = $_POST['email'];

	$result = $invService->CreateInvitation($courseid, $subject, $email);
	
	if ($result['data']['status'] == true) {
		$id = $result['data']['invitation'];
		header('Location: ManagementInvitation.php?id='.$id) ;
	} else {
		echo "It's failed while create new destination.";
	}

} else {
	
}

?>