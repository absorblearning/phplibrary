<?php

require_once('config.php');
require_once('../SCORMDispatchService.php');

global $CFG;

$ServiceUrl = $CFG->serviceurl;
$AppId = $CFG->appid;
$SecretKey = $CFG->secretkey;

$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
$courseService = $ScormService->getCourseService();

$allCourseResults = $courseService->GetCourseList();

$allResults = array();
if(isset($_GET['courseid'])){
	$courseid = $_GET['courseid'];
	$allResults = $courseService->GetRegistrationList($courseid,null);
}else{
	$allResults = $courseService->GetRegistrationList(null,null);
}

echo '<form action="CreateRegistration.php" method="GET">';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>Registration List Sample</title>
	
</head>

<body>

<h3>Create New Registration</h3>
Email: <input type="text" name="learnerid" /><br/>
First Name:<input type="text" name="learnerfirstname" /><br/>
Last Name:<input type="text" name="learnerlastname" /><br/>
<?php
if (isset($courseid)){
	echo '<input type="hidden" name="courseid" value="'.$courseid.'"/>';
}else{
	echo '<select name="courseid">';
	$courseService = $ScormService->getCourseService();
	$allCourses = $courseService->GetCourseList();
	foreach($allCourses as $course)
	{
		echo '<option value="'.$course->getCourseId().'">'.$course->getTitle().'</option>';
	}
	echo '</select><br/>';
}
?>
<input type="submit" name="submit" value="Submit" />
</form>
<br/><br/>
<br/><br/>
<?php

$redirectUrl = $CFG->wwwroot . 'RegistrationList.php';

echo '<table border="1" cellpadding="1" cellspacing="1">';
echo '<tr><td></td><td>Registration Id</td><td>Course Id</td><td>completion</td><td>success</td><td>total time</td><td>score</td><td>Launch History</td></tr>';
foreach($allResults as $result)
{
	$launchUrl = $courseService->GetLaunchUrl($result->getRegistrationId(),$redirectUrl);
	
	echo '<tr><td>';
	echo '<a class="thickbox" href="'.$launchUrl.'" >Launch</a>';
	echo '</td><td>';
	echo $result->getRegistrationId();
	echo '</td><td>';
	echo $result->getCourseId();
	echo '</td><td>';
	$regResults = $courseService->GetRegistrationResult($result->getRegistrationId());
	echo $regResults->getComplete();
	echo '</td><td>';
	echo $regResults->getSuccess();
	echo '</td><td>';
	echo $regResults->getTotalTime();
	echo '</td><td>';
	echo $regResults->getScore();
	echo '</td><td>';
	echo '<a href="LaunchHistory.php?regid='.$result->getRegistrationId().'">Launch History</a>';
	echo '</td></tr>';	
}
echo '</table><br/><br/>';

$reportUrl = "";
echo '<h3><a href="index.php">Go to Main Page.</a></h3>';
?>
</body>
</html>