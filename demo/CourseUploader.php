<?php

require_once('config.php');
require_once('../SCORMDispatchService.php');

global $CFG;

$ServiceUrl = $CFG->serviceurl;
$AppId = $CFG->appid;
$SecretKey = $CFG->secretkey;


$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
$courseService = $ScormService->getCourseService();

$courseId = uniqid();

$url = $courseService->GetUploadCourseUrl();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>Course Uploader</title>
	
</head>

<body>

	<form action="<?php echo $url; ?>" method="post" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="<?=$token?>">
	<div>
		<label for="file">Course Title:</label>
		<input type="text" name="title" id="title" />
	</div>
	<div>		
		<label for="file">Filename:</label>
		<input type="file" name="filedata" id="file" />
	</div>
	<br />
	<input type="submit" name="submit" value="Submit" />
	</form>
	<br><br>
	<h3><a href="index.php">Go to Main Page.</a></h3>
</body>
</html>