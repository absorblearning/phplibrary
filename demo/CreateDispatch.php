<?php

require_once('config.php');
require_once('../SCORMDispatchService.php');

global $CFG;

$ServiceUrl = $CFG->serviceurl;
$AppId = $CFG->appid;
$SecretKey = $CFG->secretkey;


$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
$dispService = $ScormService->getDispatchService();

$allResults = array();
if(isset($_GET['name']) && isset($_GET['cid']) && isset($_GET['did'])){
	$did = $_GET['did'];
	$cid = $_GET['cid'];
	$name = $_GET['name'];
	$limit = $_GET['limit'];
	
	$result = $dispService->CreateDispatch($name, $cid, $did, $limit);
	if ($result['data']['status'] == true) {
		$id = $result['data']['id'];
		header('Location: ManagementDispatch.php?id='.$id) ;
	} else {
		echo "It's failed while create new dispatch.";
	}

} else {
	
}

?>