<?php

require_once('config.php');
require_once('../SCORMDispatchService.php');

global $CFG;

$ServiceUrl = $CFG->serviceurl;
$AppId = $CFG->appid;
	$SecretKey = $CFG->secretkey;


	$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
	$dispService = $ScormService->getDispatchService();

	$allResults = array();
	if (isset($_GET['id'])) {

		$id = $_GET['id'];
		$result = $dispService->GetDestinationInfo($id);
		
		echo '<h3>Details of Destination</h3>';
		echo '<table border="1" cellpadding="1" cellspacing="1">';
		echo '<tr><td>Name</td><td>Created Date</td><td>Updated Date</td></tr>';
		echo '<tr><td>' . $result->getName() . '</td><td>' . $result->getCreateDate() . '</td><td>' . $result->getUpdateDate() . '</td></tr>';
		echo '</table><br/><br/>';

	} 

	$allResults = $dispService->GetDestinationList();

	echo '<form action="CreateDestination.php" method="GET">';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>Management Destination</title>
	
</head>

<body>

<h3>Create New destination</h3>
Destination Name:<input type="text" name="name" />
<input type="submit" name="submit" value="Submit" />
</form>
<br/><br/>
<br/><br/>
<?php


//$redirectUrl = 'http://localhost/SCORMDispatchApi/RegistrationList.php';
$launchUrl = '';
$i = 1;
echo '<h3>List of destination</h3>';
echo '<table border="1" cellpadding="1" cellspacing="1">';
echo '<tr><td>No.</td><td>Name</td><td>Created Date</td><td>Updated Date</td><td>Action</td></tr>';
foreach($allResults as $result)
{	
	echo '<tr>';
	echo '<td>' . $i . '</td>';
	echo '<td>' . $result->getName() . '</td>';
	echo '<td>' . $result->getCreateDate() . '</td>';
	echo '<td>' . $result->getUpdateDate() . '</td>';
	echo '<td><a href=DeleteDestination.php?id='. $result->getId() .'>Delete</td>';
	echo '</tr>';

	$i ++;
}
echo '</table><br/><br/>';

$reportUrl = "";
echo '<h3><a href="index.php">Go to Main Page.</a></h3>';
?>
</body>
</html>