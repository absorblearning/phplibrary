<?php

	require_once('config.php');
	require_once('../SCORMDispatchService.php');

	global $CFG;

	$ServiceUrl = $CFG->serviceurl;
	$AppId = $CFG->appid;
	$SecretKey = $CFG->secretkey;

	$ScormService = new SCORMDispatchService($ServiceUrl,$AppId,$SecretKey);
	$invService = $ScormService->getInvitationService();
	if (isset($_GET['id'])) {
		$invService->DeleteInvitation($_GET['id']);
	}

	header('Location: ManagementInvitation.php') ;
?>