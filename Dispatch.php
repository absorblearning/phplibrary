<?php

class Dispatch {
    private $_id;
    private $_name;
    private $_destinationId;
    private $_destinationName;
    private $_courseTitle;
    private $_courseId;
    private $_enabled;
    private $_createdBy;
    private $_createDate;
    private $_updateDate;
    private $_limit;

    public function __construct($data)
    {
		if(isset($data))
		{
	        $this->_id = (string) $data['id'];
            $this->_name = (string) $data['name'];
	        $this->_destinationId = (string) $data['client_id'];
	        $this->_destinationName = (string) $data['destTitle'];
	        $this->_courseTitle = (string) $data['courseTitle'];
	        $this->_courseId = (string) $data['course_id'];
	        $this->_enabled = (string)$data['blocked'];
            $this->_limit = (string)$data['access_limit'];
	        $this->_createdBy = (string) $data['creator_id'];
	        $this->_createDate = (string) $data['created_at'];
	        $this->_updateDate = (string) $data['updated_at'];
		}
    }
    
    public static function parseDispatchList($data)
    {
        $allResults = array();

		if (false == $data['data']['status']) {
            return $allResults;
        }
        
        foreach ($data['data']['dispatches'] as $dispatchElem)
        {
            $allResults[] = new Dispatch($dispatchElem);
        }

        return $allResults;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getDestinationId()
    {
        return $this->_destinationId;
    }

    public function getDestinationName()
    {
        return $this->_destinationName;
    }

    public function getCourseTitle()
    {
        return $this->_courseTitle;
    }

    public function getCourseId()
    {
        return $this->_courseId;
    }

    public function getEnabled()
    {
        return $this->_enabled;
    }

    public function getCreatedBy()
    {
        return $this->_createdBy;
    }

    public function getCreateDate()
    {
        return $this->_createDate;
    }

    public function getUpdateDate()
    {
        return $this->_updateDate;
    }

    public function getLimit()
    {
        return $this->_limit;
    }

}
?>
