<?php

require_once 'ServiceRequest.php';
require_once 'DispatchDestination.php';
require_once 'Dispatch.php';
require_once 'DispatchReports.php';


class DispatchService {
	
	private $_configuration = null;
	
	public function __construct($configuration) {
		$this->_configuration = $configuration;
	}
	
    public function GetDestinationList()
    {
        $request = new ServiceRequest($this->_configuration);
		
        $response = $request->CallService("dispatch/getDestinations");

        return DispatchDestination::parseDestinationList($response);
    }

    public function CreateDestination($name)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('name' => $name);

		$request->setMethodParams($params);
        $response = $request->CallService("dispatch/createDestination");        
        return $response;
    }

    public function DeleteDestination($destinationId)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('did' => $destinationId);
		$request->setMethodParams($params);
        return $request->CallService("dispatch/deleteDestination");
    }

    public function GetDestinationInfo($destinationId)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('did' => $destinationId);
		$request->setMethodParams($params);
        $response = $request->CallService("dispatch/getDestination");
               
        return new DispatchDestination($response['data']['destination']);
    }

    public function UpdateDestination($destinationId, $name = null, $tagList = null)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('did' => $destinationId);
        if($name != null) {
            $params['name'] = $name;
        }

		$request->setMethodParams($params);
        $request->CallService("dispatch/updateDestination");
    }

    public function CreateDispatch($name, $courseId, $destinationId, $limit = 10)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('name' => $name);
        $params['cid'] = $courseId;
        $params['did'] = $destinationId;
        $params['limit'] = $limit;

		$request->setMethodParams($params);
        $response = $request->CallService("dispatch/createDispatch");
        return $response;
    }

    public function GetDispatchInfo($dispatchId)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('dpid' => $dispatchId);
		$request->setMethodParams($params);
        $response = $request->CallService("dispatch/getDispatchInfo");
        return new Dispatch($response['data']['dispatch']);
    }

    public function DeleteDispatches($dispatchId)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array();

        if($dispatchId != null){
            $params['dpid'] = $dispatchId;
        }

		$request->setMethodParams($params);
        $request->CallService("dispatch/deleteDispatches");
    }

    public function GetDispatchReports($dispatchId)
    {   
        $request = new ServiceRequest($this->_configuration);
        
        if($dispatchId != null){
            $params['dpid'] = $dispatchId;
        }

        $request->setMethodParams($params);        
        $response = $request->CallService("dispatch/getHistories");
        //var_dump($response);exit;
        return DispatchReports::parseDispatchReports($response);        
    }

    public function GetDispatchList()
    {
        $request = new ServiceRequest($this->_configuration);
        $response = $request->CallService("dispatch/getDispatches");
        return Dispatch::parseDispatchList($response);
    }

    public function UpdateDispatches($destinationId = null, $courseId = null, $dispatchId = null, $tagList = null, $enabled = -1, $tagsToAdd = null, $tagsToRemove = null)
    {
        //echo "enabled = $enabled\n";
        $request = new ServiceRequest($this->_configuration);
        $params = array();
        if($destinationId != null){
            $params['did'] = $destinationId;
        }
        if($courseId != null){
            $params['cid'] = $courseId;
        }
        if($dispatchId != null){
            $params['dpid'] = $dispatchId;
        }
        if($enabled != -1){
            $params['enabled'] = ($enabled ? "true" : "false");
        }
		$request->setMethodParams($params);
        $request->CallService("dispatch/updateDispatches");
    }

    public function DownloadDispatchPackage($dispatchId, $learningType)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array();
        if($dispatchId != null){
            $params['dpid'] = $dispatchId;
        }
        if($learningType != null){
            $params['learning'] = $learningType;
        }        
        $request->setMethodParams($params);
        //$request->CallService("dispatch/updateDispatches");
        return $request->ConstructUrl("dispatch/download");
    }

    public function GetDispatchDownloadUrl($dispatchId = null, $tagList = null, $cssUrl = null)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array();
        if($destinationId != null){
            $params['did'] = $destinationId;
        }
        if($courseId != null){
            $params['cid'] = $courseId;
        }
        if($dispatchId != null){
            $params['dpid'] = $dispatchId;
        }
		$request->setMethodParams($params);
        return $request->ConstructUrl("dispatch/downloadDispatches");
    }

}
