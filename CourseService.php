<?php

require_once ('ServiceRequest.php');
require_once ('CourseData.php');
require_once ('ReportSummary.php');
require_once ('RegistrationData.php');
require_once ('LaunchHistoryData.php');


class CourseService {

	private $_configuration = null;

	public function __construct($configuration) {
		$this->_configuration = $configuration;
	}


    public function GetUploadCourseUrl()
    {
        $request = new ServiceRequest($this->_configuration);
        return $request->ConstructUrl('course/importCourse');
    }


    public function CourseUploadAsync($absoluteFilePathToZip)
    {
        $request = new ServiceRequest($this->_configuration);
        $request->setFileToPost($absoluteFilePathToZip);
        
        return $request->CallService("course/importCourse");
    }


    public function DeleteCourse($courseId)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('cid' => $courseId);

        $request->SetMethodParams($params);

        return $request->CallService("course/deleteCourse");
    }


    public function GetPreviewUrl($courseId, $redirectOnExitUrl)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('cid' => $courseId);

        if(isset($redirectOnExitUrl))
        {
            $params['url'] = $redirectOnExitUrl;
        }

        $request->SetMethodParams($params);

        return $request->ConstructUrl("course/preview");
    }


    public function GetCourseList()
    {
        $request = new ServiceRequest($this->_configuration);
        
        $response = $request->CallService("course/getCourseList");        
        $CourseDataObject = new CourseData(null);       
        return $CourseDataObject->ConvertToCourseDataList($response);
    }


    public function GetCourseSummary($courseId, $redirectOnExitUrl)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('cid' => $courseId);
        if(isset($redirectOnExitUrl))
        {
            $params['url'] = $redirectOnExitUrl;
        }            
        $request->setMethodParams($params);

        $response = $request->CallService("course/report");
        
        return new ReportSummary($response);
    }

    public function GetRegistrationList($courseId, $learnerId)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array();
        if (isset($courseId))
        {
            $params['cid'] = $courseId;
        }
        if (isset($learnerId))
        {
            $params['lid'] = $learnerId;
        }
        $request->setMethodParams($params);

        $response = $request->CallService("course/getRegistrations");
        $regData = new RegistrationData(null);
        // Return the subset of the xml starting with the top <summary>
        $regArray = $regData->ConvertToRegistrationDataList($response);

        return $regArray;
    }

    public function GetLaunchHistory($registrationId)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('rid' => $registrationId);
        $request->setMethodParams($params);
        
        $response = $request->CallService("registration/launchHistories");
        $historyData = new LaunchHistory(null);
        $historyArray = $historyData->ConvertToLaunchInfoList($response);
        return $historyArray;
    }


    public function GetRegistrationResult($registrationId)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('rid' => $registrationId);

        $request->setMethodParams($params);
        $response = $request->CallService("registration/report");

        // Return the subset of the xml starting with the top <summary>
        //return $response;
        return new ReportSummary($response);
    }

    public function CreateRegistration($registrationId, $courseId, $learnerId, $learnerFirstName, 
                                        $learnerLastName)
    {
        $request = new ServiceRequest($this->_configuration);

        $params = array('rid'=>$registrationId,
                        'cid'=>$courseId,
                        'fn'=>$learnerFirstName,
                        'ln'=>$learnerLastName,
                        'lid'=>$learnerId);
        
        $request->setMethodParams($params);
        
        return $request->CallService("registration/createRegistration");
    }

    public function GetLaunchUrl($registrationId, $redirectOnExitUrl=null)
    {
        $request = new ServiceRequest($this->_configuration);
        $params = array('rid' => $registrationId);
        
        if(isset($redirectOnExitUrl))
        {
            $params['url'] = $redirectOnExitUrl;
        }

        $request->setMethodParams($params);
        return $request->ConstructUrl("registration/launch");
    } 
 }

?>
